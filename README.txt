
helpful moudules
	- filefield_path module
	- autonodetitles



0. Set file access to private
		Administer -> Site configuration -> File system
		/admin/settings/file-system

0. Create new directory in main files directory: rcourier_temp
		example: /sites/default/files/rcourier_temp

0. Install the module
		Administer -> Site building -> Modules
		/admin/build/modules

0. Create a content type for reports (with fields: )
		Administer -› Content management -› Content types -> Add content type
		/admin/content/types/add

00. Add these fields
		- report_file (file upload field)
		- report_month (text field with dropdown widget)
		- report_year (text field with dropdown widget)
		- access_code (determines access to users)
00. Recommended: enable content_access module to limit access to and visibility of specific fields.

0. Select that content type as your report
		Administer -› Site configuration -> Report courier settings
		/admin/settings/rcourier

0. Rebuild permissions
		Administer -› Content management -> Post settings
		admin/content/node-settings

0. Set permissions for "upload rcourier reports"
		Administer -› User management -> Permissions
		/admin/user/permissions

0. Upload
		Administer -› Content management -> Report courier uploading
		/admin/content/rcourier_upload

0. Correct autonodetitles
		Administer -› Content management -> Content
		/admin/content/node

0. If views is installed, create a view for listing these uploads "My reports"