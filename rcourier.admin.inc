<?php

/**
 * @file
 * Admin page for page callbacks for Report Courier module
 */


/**
 * Form builder. Configure report courier
 * @ingroup forms
 * @see system_settings_form()
 */
function rcourier_admin_settings() {
  $options = node_get_types('names');

  $form['rcourier_node_type'] = array(
    '#type' => 'radios',
    '#title' => t('Content Type for reports'),
    '#description' => t('This content type will be used by the Report Courier module.  Unique access control will be applied to these nodes, so this should be a content type created specifically for these reports.'),
    '#options' => $options,
    '#default_value' => variable_get('rcourier_node_type', 'page'),
  );

  return system_settings_form($form);
}



// WISHLIST
// add options to select:
// - date field
// - file field
// - access code field
